<?php

namespace Greetik\BEInterfaceBundle\Service;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;
use DateTime;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */

class Tools {
    
    private $em;
    
    public function __construct($_entityManager){
        $this->em = $_entityManager;
    }
    
    public function getWeekdays($day=''){
        $days = array('1'=>'Lunes', '2'=>'Martes', '3'=>'Miércoles', '4'=>'Jueves', '5'=>'Viernes', '6'=>'Sábado', '7'=>'Domingo');
        
        if (!empty($day)){ 
            try{
                return $days[$day];            
            }catch(\Exception $e){
                return '';
            }
        }
        return $days;
    }
    
    /**
    * Order of the elems of an array, the field in db of the elem has to be "numorder"
    * 
    * @param files
    * @author Pacolmg
    */    
    public function reorderItemElems($files){

       $i=1;
        foreach($files as $k=>$file){
            $file->setNumorder($i);
            $this->em->persist($file);
            $i++;
        }
        
        $this->em->flush();        
        
        return array('errorCode'=>0);
    }
  
    public function getItemName($id, $repository){
        return $this->em->getRepository($repository)->findOneById($id)->getName();
    }
    
    /**
    * Move an elem from its old position to a new position, the elem has to have the fields: numorder, itemtype and itemid (this function is for elems of a item. Ex: the images of a post)
    * 
    * @param repository, the elem that we have to move
    * @param id of the elem to move
    * @param newposition the new position of the elem
    * @author Pacolmg
    */    
    public function moveItemElem($repository, $id, $newposition,  $oldposition='', $maxposition='', $item_type='', $item_id='', $extrafield=''){
        $file = $this->em->getRepository($repository)->findOneById($id);
        if (!$file) throw new Exception('No se encontró el elemento a mover'); 
        
        if (empty($oldposition)) $oldposition=$file->getNumorder();
        if (empty($item_type))$item_type=$file->getItemtype();
        if (empty($item_id))$item_id=$file->getItemid();

        if ($newposition<=0) $newposition = 0;
        if ($newposition>$maxposition) $newposition = $maxposition;
        $newposition++;

        
        if ($newposition!=$oldposition){
            if ($newposition<$oldposition){ //move elems to right
                for ($i=$oldposition-1; $i>=$newposition; $i--){
                    $elemtomove = $this->em->getRepository($repository)->findOneByNumorderAndElem($i, $item_type, $item_id, $id, $extrafield);
                    $elemtomove->setNumorder($i+1);
                    $this->em->persist($elemtomove);
                }
            }else{//move elems to left
                for ($i=$oldposition+1; $i<=$newposition; $i++){
                    $elemtomove = $this->em->getRepository($repository)->findOneByNumorderAndElem($i, $item_type, $item_id, $id, $extrafield);
                    $elemtomove->setNumorder($i-1);
                    $this->em->persist($elemtomove);                
                }
            }
        }
        
        $file->setNumorder($newposition);
        $this->em->persist($file);

        $this->em->flush();        
    }

    /*Check if an object is in array of objects*/
    public function isObjectInArray($object, $array){
        foreach($array as $v){
            if ($object->getId()==$v) return true;
        }
        return false;
    }
    
    /****** INTERFACE **********/
    public function dateFromPickDate($date){
        if (!$date) return new DateTime();
        $aux = explode("/", $date);
        return new DateTime(date('Y-m-d H:i:s', strtotime($aux[2].'-'.$aux[1].'-'.$aux[0].' 00:00:00')));
    }
    

    public function datetimeFromPickDatetime($date) {
        if (!$date)
            return new DateTime();
        
        list($date, $time) = explode('-', $date);
        $aux = explode("/", $date);
        
        return new DateTime(date('Y-m-d H:i:s', strtotime(trim($aux[2]) . '-' . trim($aux[1]) . '-' . trim($aux[0]).' '.trim($time))));
    }   
   
    public function getDateFromParams($date){
        if (empty($date)) return $date;
        
        if (is_numeric($date)){
            $time = $date;
            $date = new \Datetime();
            $date->setTimestamp($time);
        }
        
        if (is_string($date)){
            $date = new \Datetime($date);
        }
        
        return $date;
        
    }
    /********* AVATAR *********/
    public function changeAvatar($uploadedfile, $item, $path, $avatar2=false){
        $auxext = explode('.', $uploadedfile->getClientOriginalName());
        $ext = $auxext[count($auxext)-1];
        $filename = sha1(uniqid(mt_rand(), true)) .'.'.$ext;

        $folders = explode('\/', $path);
        $newpath = '';
        foreach($folders as $folder){
            $newpath=$folder.'/';
            if ($folder == '..' ) continue;
            if (!file_exists($newpath)){
                mkdir($newpath);            
            }
        }
        $path.=$item->getId().'/';
        if (!file_exists($path)) mkdir($path);
       
       if ($avatar2) @unlink($path.$item->getAvatar2());
       else @unlink($path.$item->getAvatar());
       
       $uploadedfile->move($path, $filename);
        
         if ($avatar2) $item->setAvatar2( $filename);
         else $item->setAvatar( $filename);
        
        $this->em->persist($item);
        $this->em->flush();
        
    }    
    
    public function deleteAvatar($item, $path, $avatar2=false){
        if ($avatar2){
            $item->setAvatar2(null);
            @unlink($path.$item->getAvatar2());
        }else{
            $item->setAvatar(null);
            @unlink($path.$item->getAvatar());
        }
        $this->em->persist($item);
        $this->em->flush();
    }
    
    /*** URLS AND SUMMARY TEXTS / SNNIPETS ***/    
    public function spaceencodeurl($url, $notildes = false) {
        if ($notildes) {
            $array_search = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "ntilde;", "Ã¡", "Ã©", "Ã­", "Ã³", "Ãº", "Ã±", "Á", "É", "Í", "Ó", "Ú", "Ñ","¿","¡", 'ä', 'ë', 'ï', 'ö', 'ü', 'Ä', 'Ë', 'Ï', 'Ö', 'Ü', " ", ".", ",", "&", "?",  "!", '+');
            foreach($array_search as $k=>$v) if ($k<12 || $k>29) $array_search[$k] = utf8_decode ($v);
            $array_replace = array('a', 'e', 'i', 'o', 'u', 'n','a', 'e', 'i', 'o', 'u', 'n','a', 'e', 'i', 'o', 'u', 'n','-', '-', 'a', 'e', 'i', 'o', 'u','a', 'e', 'i', 'o', 'u', '-', '-', '-', 'y', '-', '-', '-');
            /*
                    dump($array_search);
                    dump($array_replace);
                    dump($url);*/
            
            return preg_replace('/[^0-9a-zA-Z\-]*/','',strtolower(str_replace($array_search,$array_replace,$url)));
            //return strtolower(str_replace($array_search, $array_replace, $url));
        }

        return urlencode(str_replace(' ', '-', $url));
    }
    public function spacedecodeurl($url)
    {
        return urldecode(str_replace('-', '%20', $url));
    }    
    
    public function summarytextbywords($text, $numwords)
    {
        $summaryarray = explode(' ', strip_tags($text));
        
        return implode(' ', array_slice($summaryarray, 0, $numwords)).(($numwords>=count($summaryarray))?'':'...');
        
    }
    public function summarytextbychars($text, $numchars)
    {
        $array_search = array('&aacute;','&eacute;','&iacute;','&oacute;','&uacute;', 'ntilde;', '&nbsp;', "\n");
        $array_replace = array('á', 'é', 'í', 'ó', 'ú', 'ñ', ' ', '');
        
        return substr(str_replace($array_search, $array_replace, strip_tags($text)), 0, $numchars).(($numchars>=strlen($text))?'':'...');
    }    

    /*     * ***** users ******** */

    public function generateRandomPassword() {
        //Initialize the random password
        $password = '';

        //Initialize a random desired length
        $desired_length = rand(8, 12);

        for ($length = 0; $length < $desired_length; $length++) {
            //Append a random ASCII character (including symbols)
            $password .= chr(rand(48, 123));
        }

        return $password;
    }

    public function generateRandomUsername($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
