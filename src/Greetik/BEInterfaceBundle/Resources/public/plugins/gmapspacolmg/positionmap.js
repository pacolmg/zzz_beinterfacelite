(function (window, undefined) {
    var PositionMapLib = function (options) {
        var self = this;

        options = options || {};

        this.canvas_map = options.canvas_map || "map_canvas";

        this.environment = options.environment || "prod";

        this.urlgetmarker = options.urlgetmarkers;
        this.urlmovemarker = options.urlmovemarker;

        this.publicmap = options.publicmap || false;

        // zoom level when map is loaded (bigger is more zoomed in)
        this.defaultZoom = options.defaultZoom || 11;

        // marker image for your searched address
        this.addrMarkerImage = options.addrMarkerImage || 'images/blue-pushpin.png';


        this.myOptions = {
            zoom: this.defaultZoom,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map($("#" + this.canvas_map)[0], this.myOptions);

    };

    PositionMapLib.prototype.getMap = function(){return this.map;};

    PositionMapLib.prototype.getCenter = function(){return this.map.getCenter();};
    
    PositionMapLib.prototype.setCenter = function(center){return this.map.setCenter(center);};

    PositionMapLib.prototype.getMarker = function () {
        $.post(this.urlgetmarker, {}, function (r) {
            if (!r) {
                alert('Error Desconocido');
                return;
            }
            if (r.errorCode) {
                alert(r.errorDescription);
                return;
            }
            var latlonmarker = new google.maps.LatLng(r.data['lat'], r.data['lon']);
            this.map.setCenter(latlonmarker);
            
            this.drawMarker(r.data['id'], latlonmarker, r.data['title'], r.data['content'], r.data['color']);
        }.bind(this));
    }


    PositionMapLib.prototype.drawMarker = function (id, latlng, title, content, pinColor, pinShadow, pinImage) {

        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        pinColor = pinColor.replace('#', '');

        var marker = new google.maps.Marker({
            id: id,
            position: latlng,
            map: this.map,
            title: title,
            content: content,
            draggable: !this.publicmap,
            icon: ((pinColor != '' && typeof (pinColor) != 'undefined') ? this.getPinImage(pinColor) : ((pinImage != '' && typeof (pinImage) != 'undefined') ? pinImage : '')),
            shadow: ((pinShadow) ? this.getPinShadow() : '')
        });


        if (this.publicmap !== true) {
            google.maps.event.addListener(marker, 'dragend', function () {
                $.post(this.urlmovemarker, {'id': marker.id, 'lat': marker.getPosition().lat(), 'lon': marker.getPosition().lng()}, function (r) {
                    if (!r) {
                        alert('Error Desconocido');
                        return;
                    }
                    if (r.errorCode) {
                        alert(r.errorDescription);
                        return;
                    }

                    $("#info_infosave").html("<i class='icon-map-marker'></i> Â¡Nueva posiciÃ³n de marcador actualizada!");
                }.bind(this));
            }.bind(this));
        }

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(this.map, marker);
        });

        return marker;
    }

    PositionMapLib.prototype.openInfoWindow = function (_idmarker) {
        self = this;

        var marker = self.arraymarkers[_idmarker];

        //center the map
        /*
        var bound = new google.maps.LatLngBounds();
        bound.extend(marker.position);
        self.map.setCenter(bound.getCenter());
        self.map.fitBounds(bound);
*/
        var infowindow = new google.maps.InfoWindow({content: marker.content});
        infowindow.open(self.map, marker);
    }

    PositionMapLib.prototype.getPinImage = function (pinColor) {
        return new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                new google.maps.Size(21, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(10, 34));
    }

    PositionMapLib.prototype.getPinShadow = function () {
        return new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                new google.maps.Size(40, 37),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 35));
    }

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = PositionMapLib;
    } else if (typeof define === 'function' && define.amd) {
        define(function () {
            return PositionMapLib;
        });
    } else {
        window.PositionMapLib = PositionMapLib;
    }

})(window);